<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Mixin;

use Specification\Composite\Composite;
use Specification\Specification;

trait IsComposite
{
	use IsSpecification;

	/**
	 * Holds child Specifications
	 *
	 * @var array
	 */
	protected $specs = array();

	/**
	 * Constructor
	 *
	 * @param array $specs
	 */
	public function __construct($specs)
	{
		if (!is_array($specs)) {
			$specs = func_get_args();
		}

		$this->setSpecs($specs);
	}

	/**
	 * Returns the list of specs
	 *
	 * @return array<Specification>
	 */
	public function getSpecs()
	{
		return $this->specs;
	}

	/**
	 * Sets the specifications
	 *
	 * @param  array<Specification> $specs
	 * @throws \InvalidArgumentException
	 * @return Composite fluent
	 */
	public function setSpecs($specs)
	{
		foreach ((array)$specs as $spec) {
			/** @var Specification $spec */
			if (!$spec instanceof Specification) {
				throw new \InvalidArgumentException("Specifications must implement the Specification interface");
			}
		}

		$this->specs = (array)$specs;

		return $this;
	}

	/**
	 * Adds a specification to the list of specs
	 *
	 * @param Specification $spec
	 * @return Composite
	 */
	public function addSpec(Specification $spec)
	{
		$this->specs[] = $spec;

		return $this;
	}
}