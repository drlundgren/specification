<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Mixin;

/**
 * Provides an implementation for ValueBound objects to return their value
 *
 * @package Specification\Mixin
 */
trait IsValueBound
{
	use ReturnsKey;

	/**
	 * The value
	 *
	 * @var mixed
	 */
	protected $value;

	public function __construct($key, $value)
	{
		$this->key   = $key;
		$this->value = $value;
	}

	/**
	 * Returns the value
	 *
	 * @return string
	 */
	public function value()
	{
		return $this->value;
	}

}