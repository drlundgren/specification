<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Mixin;

/**
 * Provides the implementation of \Specification\Specification::isSatisfiedBy()
 *
 * This also does an additional check that the key exists
 *
 * @package Specification\Mixin
 */
trait IsSpecificationWithKey
{
	/**
	 * Tests an object/array against this specification
	 * This is a template method. Subclasses must override the _isSatisfiedBy()
	 * method instead of this one when implementing their logic.
	 *
	 * @param array $object an array or an object that implements the ArrayAccess interface
	 * @return bool whether or not the given object satisfies the specification
	 */
	final public function isSatisfiedBy($object)
	{
		if (!(is_array($object) || $object instanceof \ArrayAccess)) {
			return false;
		}

		return array_key_exists($this->key, $object) ? $this->testIsSatisfiedBy($object) : false;
	}
}