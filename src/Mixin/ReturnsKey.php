<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Mixin;

/**
 * Provides the implementation of \Specification\Constraint\ValueBound::key()
 *
 * @package Specification\Mixin
 */
trait ReturnsKey
{
	/**
	 * The key
	 *
	 * @var string
	 */
	protected $key;

	/**
	 * Returns the key
	 *
	 * @return mixed
	 */
	public function key()
	{
		return $this->key;
	}
}