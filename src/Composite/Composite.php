<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Composite;

use Specification\Specification;

/**
 * Composite Specification
 */
interface Composite
	extends Specification
{
	/**
	 * Returns the list of specs
	 *
	 * @return array<Specification>
	 */
	public function getSpecs();

	/**
	 * Sets the specifications
	 *
	 * @param  array<Specification> $specs
	 * @throws \InvalidArgumentException
	 * @return Composite
	 */
	public function setSpecs($specs);

	/**
	 * Adds a specification to the list of specs
	 *
	 * @param Specification $spec
	 * @return Composite
	 */
	public function addSpec(Specification $spec);
}
