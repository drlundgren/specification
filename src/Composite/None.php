<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Composite;

use Specification\Mixin\IsComposite;
use Specification\Specification;

/**
 * Specification for logical not (none)
 *
 * Satisfied if none of the specifications are true
 */
class None
	implements Composite
{
	use IsComposite;

	/**
	 * Implements the actual test for isSatisfiedBy
	 *
	 * @param array $object
	 * @return bool
	 */
	protected function testIsSatisfiedBy($object)
	{
		foreach ($this->specs as $spec) {
			/** @var Specification $spec */
			if ($spec->isSatisfiedBy($object) === true) {
				return false;
			}
		}

		return true;
	}
}
