<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

use Specification\Composite\All;

/**
 * Class Specifier
 *
 * @package Specification
 *
 * - composites
 * @method static Composite\All all(Specification...$specs)
 * @method static Composite\Any any(Specification...$specs)
 * @method static Composite\None not(Specification...$specs)
 * @method static Composite\One one(Specification...$specs)
 *
 * - constraints
 * @method static Constraint\Between between($key, $value)
 * @method static Constraint\Boolean always()
 * @method static Constraint\Boolean never()
 * @method static Constraint\Contains contains($key, $value)
 * @method static Constraint\EndsWith endsWith($key, $value)
 * @method static Constraint\EqualTo eq($key, $value)
 * @method static Constraint\GreaterThan gt($key, $value)
 * @method static Constraint\GreaterThanOrEqualTo gte($key, $value)
 * @method static Constraint\In in($key, $value)
 * @method static Constraint\IsEmpty isEmpty($key, $value)
 * @method static Constraint\IsNull isNull($key, $value)
 * @method static Constraint\LessThan lt($key, $value)
 * @method static Constraint\LessThanOrEqualTo lte($key, $value)
 * @method static Constraint\Matches matches($key, $value)
 * @method static Constraint\StartsWith startsWith($key, $value)
 */
class Specifier
	extends All
{
	/**
	 * @var Factory
	 */
	protected static $factory;

	/**
	 * Sets the factory
	 *
	 * @param Factory $factory
	 */
	public static function setFactory(Factory $factory)
	{
		static::$factory = $factory;
	}

	/**
	 * Adds a namespace to the factory
	 *
	 * @param string $namespace
	 * @param bool   $prepend
	 */
	public static function with($namespace, $prepend = false)
	{
		if ($prepend) {
			static::getFactory()->appendNamespace($namespace);
		}
		else {
			static::getFactory()->prependNamespace($namespace);
		}
	}

	/**
	 * Calls the appropriate method
	 *
	 * @magic
	 * @param $name
	 * @param $arguments
	 * @return Specification
	 */
	public static function __callStatic($name, $arguments)
	{
		return static::buildSpecification($name, $arguments);
	}

	/**
	 * Builds the specification
	 *
	 * @param string|Specification $spec
	 * @param array                $arguments
	 * @return Specification
	 */
	public static function buildSpecification($spec, $arguments = [])
	{
		return static::getFactory()->newSpecification($spec, $arguments);
	}

	/**
	 * Returns the factory
	 *
	 * @return Factory
	 */
	protected static function getFactory()
	{
		if (!static::$factory instanceof Factory) {
			static::$factory = new Factory();
		}

		return static::$factory;
	}
}
