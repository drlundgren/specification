<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

use Specification\Composite\All;
use Specification\Composite\Any;
use Specification\Composite\None;
use Specification\Composite\One;
use Specification\Constraint\Between;
use Specification\Constraint\Contains;
use Specification\Constraint\EndsWith;
use Specification\Constraint\EqualTo;
use Specification\Constraint\GreaterThan;
use Specification\Constraint\GreaterThanOrEqualTo;
use Specification\Constraint\In;
use Specification\Constraint\LessThan;
use Specification\Constraint\LessThanOrEqualTo;
use Specification\Constraint\Matches;
use Specification\Constraint\StartsWith;

/**
 * Factory class for instances of Specifications
 *
 * The order of searching for concrete implementations is as follows:
 * - aliases
 * - specs
 * - prefixes (namespaces)
 *
 * @package Specification
 */
class Factory
{
	/**
	 * List of namespace prefixes
	 *
	 * @var array
	 */
	protected $namespaces = [];

	/**
	 * List of specifications
	 *
	 * @var array
	 */
	protected $specs = [
		// composite (alternate forms of and, or, not, xor)
		'all'        => All::class,
		'any'        => Any::class,
		'not'        => None::class,
		'none'       => None::class,
		'one'        => One::class,
		// constraint
		'lt'         => LessThan::class,
		'lte'        => LessThanOrEqualTo::class,
		'gt'         => GreaterThan::class,
		'gte'        => GreaterThanOrEqualTo::class,
		'eq'         => EqualTo::class,
		'between'    => Between::class,
		'contains'   => Contains::class,
		'endsWith'   => EndsWith::class,
		'startsWith' => StartsWith::class,
		'matches'    => Matches::class,
		'in'         => In::class,
	];

	/**
	 * List of shortcuts for the constraints
	 *
	 * @var array
	 */
	protected $aliases = [
		// composite
		'andx'    => 'all',
		'orx'     => 'any',
		'xorx'    => 'one',
		'notx'    => 'not',

		// constraint
		'equals'  => 'eq',
		'equalTo' => 'eq',
	];

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->namespaces = [
			__NAMESPACE__ . '\\Composite\\',
			__NAMESPACE__ . '\\Constraint\\'
		];
	}

	/**
	 * Appends a namespace
	 *
	 * @param string $prefix
	 */
	public function appendNamespace($prefix)
	{
		$this->namespaces[] = $this->normalizeNamespace($prefix);
	}

	/**
	 * Prepends a namespace
	 *
	 * @param $prefix
	 */
	public function prependNamespace($prefix)
	{
		array_unshift($this->namespaces, $this->normalizeNamespace($prefix));
	}

	/**
	 * Sets an alias to a spec
	 *
	 * @param string $alias
	 * @param string $spec
	 */
	public function setAlias($alias, $spec)
	{
		$this->aliases[$alias] = isset($this->specs[$spec])
			? $this->specs[$spec]
			: $spec;
	}

	/**
	 * Sets the specification
	 *
	 * @param string $name
	 * @param string $spec
	 */
	public function setSpecification($name, $spec)
	{
		if (!class_exists($spec)) {
			throw new \InvalidArgumentException("Specification class does not exist: $spec");
		}

		$this->specs[$name] = $spec;
	}

	/**
	 * Returns a created specification
	 *
	 * @param string $name
	 * @param array  $arguments
	 * @return Specification
	 */
	public function newSpecification($name, $arguments)
	{
		if ($name instanceof Specification) {
			return $name;
		}

		return $this->initSpec($this->resolveSpecName($name), $arguments);
	}

	/**
	 * Resolves the name of the specification
	 *
	 * @param string $name
	 * @return string
	 */
	private function resolveSpecName($name)
	{
		while (isset($this->aliases[$name])) {
			$name = $this->aliases[$name];
		}

		if (isset($this->specs[$name])) {
			return $this->specs[$name];
		}

		if (strpos($name, '\\') !== false && class_exists($name)) {
			return $name;
		}

		$name = ucfirst($name);
		foreach ($this->namespaces as $prefix) {
			$class = "{$prefix}{$name}";
			if (!class_exists($class)) {
				continue;
			}

			return $class;
		}

		throw new \InvalidArgumentException("Specification could not be found: $name");
	}

	/**
	 * Initializes the specification
	 *
	 * @param string $class
	 * @param array|Specification $args
	 * @return Specification
	 */
	private function initSpec($class, $args)
	{
		if (!class_exists($class)) {
			throw new \InvalidArgumentException("Specification could not be found: $class");
		}

		$reflection = new \ReflectionClass($class);
		if ($reflection->implementsInterface(Specification::class) === false) {
			throw new \RuntimeException(sprintf('"%s" is not a valid specification', $class));
		}

		return $reflection->getConstructor()
			? $reflection->newInstanceArgs($args)
			: new $class;
	}

	/**
	 * Ensures that the prefix has \\ at the end
	 *
	 * @param $prefix
	 * @return string
	 */
	protected function normalizeNamespace($prefix)
	{
		return rtrim($prefix, '\\') . '\\';
	}
}