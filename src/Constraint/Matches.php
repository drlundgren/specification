<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

use Specification\Mixin\IsSpecificationWithKey;
use Specification\Mixin\IsValueBound;
use Specification\Specification;

/**
 * Satisfied when the value matches the regular expression
 */
class Matches
	implements Specification, ValueBound
{
	use IsSpecificationWithKey, IsValueBound;

	/**
	 * The regex used in the comparison
	 *
	 * @var string
	 */
	private $regex;

	public function __construct($key, $value)
	{
		$this->key   = $key;
		$this->value = $value;
		$this->regex = $this->normalizeRegex($value);
	}

	/**
	 * Returns whether or not the string is alphanumeric
	 *
	 * @param $str
	 * @return bool
	 */
	protected function isValidDelimiter($str)
	{
		if ($str === '\\') {
			return false;
		}
		elseif (function_exists('ctype_alnum')) {
			$result = ctype_alnum($str);
		}
		else {
			$result = (bool)preg_match('/^[a-zA-Z0-9]+$/', $str);
		}

		return !$result;
	}

	/**
	 * Normalizes the Regex to ensure it is more valid
	 *
	 * @param $value
	 * @return string
	 */
	protected function normalizeRegex($value)
	{

		return $this->isValidDelimiter($value[0]) ? $value : '/' . str_replace('/', '\/', $value) . '/';
	}

	/**
	 * Implements the actual test for isSatisfiedBy
	 *
	 * @param array $object
	 * @return bool
	 */
	protected function testIsSatisfiedBy($object)
	{
		$matched = @preg_match($this->regex, $object[$this->key]);
		if ($matched === false) {
			throw new \RuntimeException("preg_match failed: " . preg_last_error());
		}

		return (bool)$matched;
	}
}
