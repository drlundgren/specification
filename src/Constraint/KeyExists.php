<?php
namespace Specification\Constraint;

use Specification\Mixin\IsSpecification;
use Specification\Mixin\ReturnsKey;
use Specification\Specification;

/**
 * Satisfied when the value is anything
 */
class KeyExists
	implements Specification, ValueBound
{
	use IsSpecification, ReturnsKey;

	public function __construct($key)
	{
		$this->key = $key;
	}

	/**
	 * Implements the actual test for isSatisfiedBy
	 *
	 * @param array $object
	 * @return bool
	 */
	protected function testIsSatisfiedBy($object)
	{
		return array_key_exists($this->key, $object);
	}
}