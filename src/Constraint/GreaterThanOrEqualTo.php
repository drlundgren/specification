<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

use Specification\Mixin\IsSpecificationWithKey;
use Specification\Mixin\IsValueBound;
use Specification\Specification;

/**
 * Satisfied when the value is greater than or equal to
 */
class GreaterThanOrEqualTo
	implements Specification, ValueBound
{
	use IsSpecificationWithKey, IsValueBound;

	/**
	 * Implements the actual test for isSatisfiedBy
	 *
	 * @param array $object
	 * @return bool
	 */
	protected function testIsSatisfiedBy($object)
	{
		return $object[$this->key] >= $this->value;
	}
}
