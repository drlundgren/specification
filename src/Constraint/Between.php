<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

use Specification\Mixin\IsSpecificationWithKey;
use Specification\Mixin\ReturnsKey;
use Specification\Specification;

/**
 * Satisfied if the value is between min and max
 *
 * This may be used with DateTime objects as well
 */
class Between
	implements Specification, ValueBound
{
	use IsSpecificationWithKey, ReturnsKey;

	/**
	 * @var integer
	 */
	protected $min;

	/**
	 * @var integer
	 */
	protected $max;

	/**
	 * @var bool
	 */
	protected $inclusive = true;

	/**
	 * @param mixed $key
	 * @param mixed $min
	 * @param       $max
	 * @param bool  $inclusive
	 */
	public function __construct($key, $min, $max, $inclusive = true)
	{
		$this->key       = $key;
		$this->min       = $min;
		$this->max       = $max;
		$this->inclusive = $inclusive;
	}

	/**
	 * Tests that the object value is between the min/max values
	 *
	 * @param array $object
	 * @return bool
	 */
	protected function testIsSatisfiedBy($object)
	{
		$value = $object[$this->key];
		if ($this->inclusive && ($value >= $this->min && $value <= $this->max)) {
			return true;
		}
		elseif ($value > $this->min && $value < $this->max) {
			return true;
		}

		return false;
	}
}
