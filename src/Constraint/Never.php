<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

use Specification\Specification;

/**
 * Never satisfied
 */
class Never
	implements Specification
{
	/**
	 * Implements \Specification\Specification::isSatisfiedBy()
	 *
	 * @param array $object
	 * @return bool
	 */
	public function isSatisfiedBy($object)
	{
		return false;
	}
}
