<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

/**
 * Contract for Specifications
 *
 * @author David Lundgren
 */
interface Specification
{
	/**
	 * Tests an object/array against this specification
	 *
	 * @param array|\ArrayAccess $object an array or an object that implements the ArrayAccess interface
	 * @return bool whether or not the given object satisfies the specification
	 */
	public function isSatisfiedBy($object);
}
