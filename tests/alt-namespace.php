<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace AlternateSpecification;

use Specification\Specification;
use Specification\Mixin\IsSpecification;

class Factory
	extends \Specification\Factory
{
	public function __construct()
	{
		$this->specs = [
			'all' => Mine::class
		];
		$this->aliases = [
			'woot' => 'all'
		];
	}
}

class Mine
	implements Specification
{
	use IsSpecification;

	/**
	 * @codeCoverageIgnore
	 * @param array $object
	 * @return bool
	 */
	public function testIsSatisfiedBy($object)
	{
		return true;
	}
}

class EqualTo extends Mine
{}