<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

use Specification\Specifier as spec;


class FunctionalTest
	extends \PHPUnit_Framework_TestCase
{
	public function testExample()
	{

		$jediMaster = spec::all(
			spec::eq('occupation', 'Jedi'),
			spec::eq('level', 'Master')
		);

		$leia      = array('name' => 'Shanti', 'occupation' => 'Jedi', 'level' => 'Padawan');
		$uncleOwen = array('name' => 'Uncle Owen', 'occupation' => 'Farmer');
		$luke      = array('name' => 'Luke Skywalker', 'occupation' => 'Jedi', 'level' => 'Master');

//		self::assertFalse($jediMaster->isSatisfiedBy($uncleOwen));
//		self::assertTrue($jediMaster->isSatisfiedBy($luke));
//		self::assertFalse($jediMaster->isSatisfiedBy($leia));



	}
}