<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Composite;
use Specification\Constraint\EqualTo;

class AllTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Provides data to be used by other tests
	 *
	 * @return array
	 */
	public function getProvider()
	{
		$composite = array(
			new EqualTo('role', 'Padawan'),
			new EqualTo('name', 'Luke Skywalker')
		);
		return array(
			// array() -- composite, test, expected
			array($composite, array('name' => 'Luke Skywalker'), false),
			array($composite, array('role' => 'Padawan'), false),
			array($composite, array('name' => 'Luke Skywalker', 'role' => 'Padawan'), true),
			array($composite, array('name' => 'Luke Skywalker', 'role' => 'Jedi Master'), false),
		);
	}

	/**
	 * @dataProvider getProvider
	 */
	public function testSatisifiedBy($composite, $test, $expected)
	{
		$tester = new All($composite);

		self::assertEquals($expected, $tester->isSatisfiedBy($test));
	}
}
