<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

use AlternateSpecification\Factory as AltFactory;
use AlternateSpecification\Mine;
use AlternateSpecification\EqualTo as AltEqualTo;
use Specification\Composite\All;

class SpecifierTest
	extends \PHPUnit_Framework_TestCase
{
	public function testDefaultFactoryIsUsed()
	{
		self::assertInstanceOf(All::class, Specifier::all(Specifier::always()));
	}

	public function testCustomFactoryIsUsed()
	{
		Specifier::setFactory(new AltFactory());
		self::assertInstanceOf(Mine::class, Specifier::all());
	}

	public function testWithAppendsNamespace()
	{
		Specifier::with('AlternateSpecification\\');
		self::assertInstanceOf(Mine::class, Specifier::mine());
	}

	public function testWithPrependsNamespace()
	{

		Specifier::with('AlternateSpecification\\', true);
		self::assertInstanceOf(AltEqualTo::class, Specifier::equalTo('key', 'value'));
	}
}