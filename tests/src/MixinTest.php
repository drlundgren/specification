<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

use AlternateSpecification\Mine;
use AlternateSpecification\EqualTo as AltEqualTo;
use Specification\Composite\All;
use Specification\Constraint\Always;
use Specification\Constraint\EqualTo;

/**
 * Code Coverage tests for the mixin's
 *
 * @package Specification
 */
class MixinTest
	extends \PHPUnit_Framework_TestCase
{
	public function testValueBound()
	{
		$et = new EqualTo('key', 'value');
		self::assertEquals('key', $et->key());
		self::assertEquals('value', $et->value());
	}

	public function testIsSpecificationIsSatisfiedByReturnsFalse()
	{
		$et = new All([new Always()]);
		self::assertFalse($et->isSatisfiedBy('kakaw'));
	}

	public function testIsSpecificationWithKeyIsSatisfiedByReturnsFalse()
	{
		$et = new EqualTo('k', 'v');
		self::assertFalse($et->isSatisfiedBy('kakaw'));
	}

	/**
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Specifications must implement the Specification interface
	 */
	public function testIsCompositeSetSpecsThrowsException()
	{
		new All('k');
	}

	public function testIsCompositeGetSpecsReturnsSpecs()
	{
		$specs = [
			new Always()
		];
		self::assertEquals($specs, (new All($specs))->getSpecs());
	}

	public function testIsCompositeAddSpec()
	{
		$specs   = [
			new Always()
		];
		$all     = new All($specs);
		$specs[] = new Always();

		self::assertInstanceOf(All::class, $all->addSpec($specs[1]));
		self::assertEquals($specs, $all->getSpecs());
	}
}