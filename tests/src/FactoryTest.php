<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification;

use AlternateSpecification\Mine;
use AlternateSpecification\Factory as AltFactory;
use Specification\Constraint\Always;
use Specification\Constraint\EqualTo;

class FactoryTest
	extends \PHPUnit_Framework_TestCase
{
	public function testSpecificationResolution()
	{
		$factory = new Factory();
		self::assertInstanceOf(Always::class, $factory->newSpecification('always', ['kakaw']));
	}

	public function testAliasResolution()
	{
		$factory = new Factory();
		self::assertInstanceOf(EqualTo::class, $factory->newSpecification('equals', ['key', 'value']));
	}

	public function testSetAliasResolvesAlias()
	{
		$factory = new Factory();
		$factory->setAlias('blah', 'always');
		self::assertInstanceOf(Always::class, $factory->newSpecification('blah', ['kakaw']));
	}

	public function testSetAliasWithoutResolution()
	{
		$factory = new Factory();
		$factory->setAlias('blah', Mine::class);
		self::assertInstanceOf(Mine::class, $factory->newSpecification('blah', ['kakaw']));
	}

	public function testSetSpecification()
	{
		$factory = new Factory();
		$factory->setSpecification('mine', Mine::class);
		self::assertInstanceOf(Mine::class, $factory->newSpecification('mine', ['key', 'value']));
	}

	/**
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Specification could not be found: Test
	 */
	public function testExceptionThrownOnInvalidSpecification()
	{
		(new Factory)->newSpecification('test', ['key', 'value']);
	}

	/**
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Specification class does not exist: Test\Test
	 */
	public function testExceptionThrownOnNonExistentSpecification()
	{
		(new Factory)->setSpecification('test', 'Test\Test');
	}

	public function testNewSpecificationReturnsExistingSpecificationWhenPassedIn()
	{
		$always = new Always();
		self::assertSame($always, (new Factory)->newSpecification($always, null));
	}

	/**
	 * @expectedException \InvalidArgumentException
	 * @expectedExceptionMessage Specification could not be found: Test\Test
	 */
	public function testNewSpecificationThrowsExceptionWithInvalidClass()
	{
		(new Factory)->newSpecification("Test\\Test", null);
	}

	/**
	 * @expectedException \RuntimeException
	 * @expectedExceptionMessage "AlternateSpecification\Factory" is not a valid specification
	 */
	public function testNewSpecificationThrowsExceptionWhenClassDoesNotImplementSpecification()
	{
		(new Factory)->newSpecification(AltFactory::class, null);
	}
}