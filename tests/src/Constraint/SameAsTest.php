<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the IsNull constraint
 *
 * @package Specification\Value
 */
class SameAsTest
	extends \PHPUnit_Framework_TestCase
{
	public function provideValidComparisons()
	{
		$obj        = new \stdClass();
		$obj->kakaw = 'kakaw';

		return [
			['kakaw', 'kakaw'],
			[$obj, $obj]
		];
	}

	/**
	 * @dataProvider provideValidComparisons
	 */
	public function testIsSatisfiedReturnsTrue($expected, $value)
	{
		self::assertTrue((new SameAs('kakaw', $expected))->isSatisfiedBy(['kakaw' => $value]));
	}

	public function testIsSatisifiedReturnsFalse()
	{
		self::assertFalse((new SameAs('kakaw', 'kakaw'))->isSatisfiedBy(['kakaw' => 1]));
	}
}
