<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the IsNull constraint
 *
 * @package Specification\Value
 */
class IsNullTest
	extends \PHPUnit_Framework_TestCase
{

	public function testIsSatisfiedReturnsFalse()
	{
		self::assertFalse((new IsNull('kakaw'))->isSatisfiedBy(['kakaw' => 'kakaw']));
	}

	public function testSatisifiedReturnsTrue()
	{
		self::assertTrue((new IsNull('kakaw'))->isSatisfiedBy(['kakaw' => null]));
	}
}
