<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

class GreaterThanOrEqualToTest extends \PHPUnit_Framework_TestCase
{

	public function testSatisfiedByIsTrueWithArrayAndGreaterValue()
	{
		$test = new GreaterThanOrEqualTo('age', 20);

		self::assertTrue($test->isSatisfiedBy(array('age' => 21)));
	}

	public function testSatisfiedByIsTrueWithArrayAndEqualValue()
	{
		$test = new GreaterThanOrEqualTo('age', 20);

		self::assertTrue($test->isSatisfiedBy(array('age' => 20)));
	}

	public function testSatisfiedByFailsWithArrayAndLesserValue()
	{
		$test = new GreaterThanOrEqualTo('age', 20);

		self::assertFalse($test->isSatisfiedBy(array('age' => 19)));
	}

	public function testSatisfiedByIsTrueWithArrayObjectAndGreaterValue()
	{
		$test = new GreaterThanOrEqualTo('age', 20);

		$obj = new \ArrayObject(array('age' => 21));

		self::assertTrue($test->isSatisfiedBy($obj));
	}

	public function testSatisfiedByIsTrueWithArrayObjectAndEqualValue()
	{
		$test = new GreaterThanOrEqualTo('age', 20);

		$obj = new \ArrayObject(array('age' => 20));

		self::assertTrue($test->isSatisfiedBy($obj));
	}

	public function testSatisfiedByFailsWithArrayObjectAndLesserValue()
	{
		$test = new GreaterThanOrEqualTo('age', 20);

		$obj = new \ArrayObject(array('age' => 19));

		self::assertFalse($test->isSatisfiedBy($obj));
	}
}
