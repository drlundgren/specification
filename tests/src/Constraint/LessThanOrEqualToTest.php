<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

class LessThanOrEqualToTest extends \PHPUnit_Framework_TestCase
{
	/**
	 * Provides data to be used by other tests
	 *
	 * @return array
	 */
	public function getProvider()
	{
		return array(
			array('age', 20, 'age', 19, true),
			array('age', 20, 'age', 20, true),
			array('age', 20, 'age', 22, false),
			array('age', 20, 'noage', 20, false),
		);
	}

	/**
	 * @dataProvider getProvider
	 */
	public function testSatisifiedByWithArray($k1, $v1, $k2, $v2, $expected)
	{
		$test = new LessThanOrEqualTo($k1, $v1);

		self::assertEquals($expected, $test->isSatisfiedBy(array($k2 => $v2)));
	}

	/**
	 * @dataProvider getProvider
	 */
	public function testSatisifiedByWithArrayObject($k1, $v1, $k2, $v2, $expected)
	{
		$test = new LessThanOrEqualTo($k1, $v1);

		$obj = new \ArrayObject(array($k2 => $v2));

		self::assertEquals($expected, $test->isSatisfiedBy($obj));
	}
}
