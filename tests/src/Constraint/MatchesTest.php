<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the Between valuebound specification
 *
 * @package Specification\Value
 */
class MatchesTest
	extends \PHPUnit_Framework_TestCase
{
	public function provideInvalidDelimiters()
	{
		return [
			['tes(t)?', 'test'],
			['\test', "\test"],
			['\\\\test', "\\test"]
		];
	}

	public function provideValidValues()
	{
			return [
				['testString'],
				['anothertest'],
				['test'],
				['testtest'],
			];
	}

	public function provideInvalidValues()
	{
		return [
			['xest'],
			['est'],
			['tesx'],
		];
	}

	/** @dataProvider provideValidValues */
	public function testValidComparison($str)
	{
		self::assertTrue((new Matches('kakaw', 'test'))->isSatisfiedBy(array('kakaw' => $str)), "Failed Finding 'test' in {$str}");
	}

	/** @dataProvider provideInvalidValues */
	public function testInvalidComparison($str)
	{
		self::assertFalse((new Matches('kakaw', 'test'))->isSatisfiedBy(array('kakaw' => $str)), "Failed Value: {$str}");
	}

	/**
	 * @expectedException \RuntimeException
	 * @expectedExceptionMessageRegExp /^preg_match failed:/
	 */
	public function testRegexFailureThrowsException()
	{
		$m = new Matches('kakaw', '/tes(t/');
		$m->isSatisfiedBy(['kakaw' => 'test']);
	}

	/** @dataProvider provideInvalidDelimiters */
	public function testInvalidDelimitersReturnTrue($regex, $payload)
	{
		$m = new Matches('kakaw', $regex);
		self::assertTrue($m->isSatisfiedBy(['kakaw' => $payload]));
	}

	/** @runInSeparateProcess */
	public function testIsValidDelimiterRegex()
	{
		require_once __DIR__ . '/matches-function_exists.php';
		$m = new Matches('kakaw', 'tes(t)?');
		self::assertTrue($m->isSatisfiedBy(['kakaw' => 'test']));
	}
}
