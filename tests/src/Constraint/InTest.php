<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the In value specification
 *
 * @package Specification\Value
 */
class InTest
	extends \PHPUnit_Framework_TestCase
{
	private $testArray = [
		'testString',
		'anothertest',
		'test',
		'testtest',
		'kakaw'
	];

	public function provideValidValues()
	{
		return [
			['testString'],
			['anothertest'],
			['test'],
			['testtest'],
		];
	}

	public function provideInvalidValues()
	{
		return [
			['xest'],
			['est'],
			['tesx'],
		];
	}

	/**
	 * @dataProvider provideValidValues
	 */
	public function testValidComparison($str)
	{
		self::assertTrue((new In('kakaw', $this->testArray))->isSatisfiedBy(array('kakaw' => $str)));
	}

	/**
	 * @dataProvider provideInvalidValues
	 */
	public function testInvalidComparison($str)
	{
		self::assertFalse((new In('kakaw', $this->testArray))->isSatisfiedBy(array('kakaw' => $str)));
	}
}
