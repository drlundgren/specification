<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the Between valuebound specification
 *
 * @package Specification\Value
 */
class EndsWithTest
	extends \PHPUnit_Framework_TestCase
{
	public function provideInvalidValues()
	{
			return [
				['testString'],
				['Test Of another testing'],
				['tes'],
				['text']
			];
	}

	public function provideValidValues()
	{
		return [
			['xest woot test'],
			['est test'],
			['tesx test test test'],
			['testtest'],

		];
	}

	/**
	 * @dataProvider provideValidValues
	 */
	public function testValidComparison($str)
	{
		self::assertTrue((new EndsWith('kakaw', 'test'))->isSatisfiedBy(array('kakaw' => $str)));
	}

	/**
	 * @dataProvider provideInvalidValues
	 */
	public function testInvalidComparison($str)
	{
		self::assertFalse((new EndsWith('kakaw', 'test'))->isSatisfiedBy(array('kakaw' => $str)));
	}
}
