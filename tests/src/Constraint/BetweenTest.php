<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the Between valuebound specification
 *
 * @package Specification\Value
 */
class BetweenTest
	extends \PHPUnit_Framework_TestCase
{
	/**
	 * Provides data to be used by other tests
	 *
	 * @return array
	 */
	public function getProvider()
	{
		return array(
			// inclusive
			array('count', 10, 20, true, 'count', 5, false),
			array('count', 10, 20, true, 'count', 10, true),
			array('count', 10, 20, true, 'count', 15, true),
			array('count', 10, 20, true, 'count', 20, true),
			array('count', 10, 20, true, 'count', 25, false),
			// not-inclusive
			array('count', 10, 20, false, 'count', 5, false),
			array('count', 10, 20, false, 'count', 10, false),
			array('count', 10, 20, false, 'count', 15, true),
			array('count', 10, 20, false, 'count', 20, false),
			array('count', 10, 20, false, 'count', 25, false),
		);
	}

	/**
	 * @dataProvider getProvider
	 */
	public function testSatisifiedByWithArray($k1, $min, $max, $inclusive, $k2, $v2, $expected)
	{
		$test = new Between($k1, $min, $max, $inclusive);

		self::assertEquals($expected, $test->isSatisfiedBy(array($k2 => $v2)));
	}

	/**
	 * @dataProvider getProvider
	 */
	public function testSatisifiedByWithArrayObject($k1, $min, $max, $inclusive, $k2, $v2, $expected)
	{
		$test = new Between($k1, $min, $max, $inclusive);

		$obj = new \ArrayObject(array($k2 => $v2));

		self::assertEquals($expected, $test->isSatisfiedBy($obj));
	}
}
