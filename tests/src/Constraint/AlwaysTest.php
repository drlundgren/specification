<?php
/**
 * This file is part of Specification.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright 2013-2016 David Lundgren <dlundgren@syberisle.net>
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      http://github.com/dlundgren/php-specification
 */

namespace Specification\Constraint;

/**
 * Tests the Between valuebound specification
 *
 * @package Specification\Value
 */
class AlwaysTest
	extends \PHPUnit_Framework_TestCase
{

	public function testSatisifiedByWithArray()
	{
		self::assertTrue((new Always())->isSatisfiedBy(['kakaw' => 'woot']));
	}

	public function testSatisifiedByWithObject()
	{
		self::assertTrue((new Always())->isSatisfiedBy(new \stdClass()));
	}

	public function testSatisifiedByWithAnyValue()
	{
		self::assertTrue((new Always())->isSatisfiedBy(true));
	}
}
