Specification for PHP
=================

This package contains tools to describe and use specifications in PHP. It allows
you to query the candidate objects to see if they satisfy the criteria in the
specification. The specifications may be chained together to express complex criteria.

You may also be interested in dlundgren/specification-adapters, which contains
adapters for various applications of the specifications.

This package is compliant with [PSR-1][], [PSR-4][]. If you notice compliance
oversights, please send a patch via pull request.

[PSR-1]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md
[PSR-4]: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4.md

Links
-----

Specifications by Eric Evans and Martin Fowler: http://martinfowler.com/apsupp/spec.pdf

Example
-------

```php
<?php
use Specification\Specifier as spec;

$jediMaster = spec::all(
   spec::eq('occupation', 'Jedi'),
   spec::eq('level', 'Master')
);

$leia    = array('name' => 'Shanti', 'occupation' => 'Jedi', 'level' => 'Padawan');
$uncleOwen = array('name' => 'Uncle Owen', 'occupation' => 'Farmer');
$luke  = array('name' => 'Luke Skywalker', 'occupation' => 'Jedi', 'level' => 'Master');

$jediMaster->isSatisfiedBy($uncleOwen); // false
$jediMaster->isSatisfiedBy($luke); // true
$jediMaster->isSatisfiedBy($leia); // false

?>
```
