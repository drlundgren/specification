<?php
require dirname(__DIR__) . '/vendor/autoload.php';

use Specification\Specifier as spec;

$jediMaster = spec::all(
	spec::eq('occupation', 'Jedi'),
	spec::eq('level', 'Master')
);

$leia      = array('name' => 'Shanti', 'occupation' => 'Jedi', 'level' => 'Padawan');
$uncleOwen = array('name' => 'Uncle Owen', 'occupation' => 'Farmer');
$luke      = array('name' => 'Luke Skywalker', 'occupation' => 'Jedi', 'level' => 'Master');

echo "Uncle Owen is a jedi master = " . ($jediMaster->isSatisfiedBy($uncleOwen) ? 'yes' : 'no') . "\n";
echo "Luke is a jedi master = " . ($jediMaster->isSatisfiedBy($luke) ? 'yes' : 'no') . "\n";
echo "Leia is a jedi master = " . ($jediMaster->isSatisfiedBy($leia) ? 'yes' : 'no') . "\n";

